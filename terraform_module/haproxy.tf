resource "helm_release" "haproxy" {
  chart           = "kubernetes-ingress"
  repository      = "https://haproxytech.github.io/helm-charts/"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history
  timeout         = var.helm_timeout

  values = concat([
    file("${path.module}/haproxy.yaml"),
  ], var.values)

  set {
    name  = "controller.image.repository"
    value = var.image_repository
  }
  set {
    name  = "controller.image.tag"
    value = var.image_tag
  }

  dynamic "set" {
    for_each = compact([var.limits_cpu])
    content {
      name  = "resources.limits.cpu"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = compact([var.limits_memory])
    content {
      name  = "resources.limits.memory"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = compact([var.requests_cpu])
    content {
      name  = "resources.requests.cpu"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = compact([var.requests_memory])
    content {
      name  = "resources.requests.memory"
      value = set.value
    }
  }

  set {
    name  = "controller.replicaCount"
    value = var.haproxy_replicas
  }
  set {
    name  = "defaultBackend.replicaCount"
    value = var.haproxy_replicas_default_backend
  }

  set {
    name  = "controller.service.loadBalancerIP"
    value = var.haproxy_lb_ip
  }

  set {
    name  = "controller.ingressClass"
    value = var.haproxy_ingress_class
  }
  dynamic "set" {
    for_each = var.haproxy_ignore_ingress_without_class == true ? [1] : []
    content {
      name  = "controller.extraArgs"
      value = "{\"--ignore-ingress-without-class\"}"
    }
  }

  set {
    name  = "controller.service.type"
    value = var.haproxy_service_type
  }
  set {
    name  = "controller.service.enablePorts.http"
    value = var.haproxy_service_enable_http
  }
  set {
    name  = "controller.service.enablePorts.https"
    value = var.haproxy_service_enable_https
  }
  set {
    name  = "controller.service.enablePorts.stat"
    value = var.haproxy_service_enable_stat
  }

  dynamic "set" {
    for_each = length(var.haproxy_config_response_set_header) == 0 ? [] : [1]
    content {
      name  = "controller.config.response-set-header"
      value = templatefile("${path.module}/headers.yaml.tftpl", { headers = var.haproxy_config_response_set_header })
    }
  }
}
