variable "haproxy_replicas" {
  type    = number
  default = 1
}
variable "haproxy_replicas_default_backend" {
  type    = number
  default = 1
}

variable "haproxy_lb_ip" {
  type = string
}

variable "haproxy_ingress_class" {
  type    = string
  default = "haproxy"
}
variable "haproxy_ignore_ingress_without_class" {
  type    = bool
  default = true
}

variable "haproxy_service_type" {
  type    = string
  default = "LoadBalancer"
}
variable "haproxy_service_enable_http" {
  type    = bool
  default = true
}
variable "haproxy_service_enable_https" {
  type    = bool
  default = true
}
variable "haproxy_service_enable_stat" {
  type    = bool
  default = false
}

variable "haproxy_config_response_set_header" {
  type    = list(object({ name = string, value = string }))
  default = []
}
